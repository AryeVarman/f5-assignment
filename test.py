import pytest
import pageChecker

@pytest.fixture()
def url(pytestconfig):
    return pytestconfig.getoption("url")

@pytest.fixture()
def string(pytestconfig):
    return pytestconfig.getoption("string")

def test(url, string):
    assert pageChecker.isStringInWebsite(url, string)

