import re
import urllib.request

def isStringInWebsite(url, stringToFind):
    encoding = 'utf-8'
    website = urllib.request.urlopen(url.strip()).read().decode(encoding)
    website = separateContentFromHTML(website)
    return stringToFind.strip() in website


def separateContentFromHTML(htmlAsString):
    htmlAsString = removeAllThatIsNotHTMLBody(htmlAsString)
    htmlAsString = removeScripts(htmlAsString)
    htmlAsString = removeHTMLTagsFromText(htmlAsString)
    return htmlAsString


def removeAllThatIsNotHTMLBody(htmlAsString):
    startOfBody = htmlAsString.find("<body")
    endOfBody = htmlAsString.find("</body")

    if startOfBody != -1 and endOfBody != -1:
        htmlAsString = htmlAsString[startOfBody:endOfBody]

    return htmlAsString


def removeScripts(htmlAsString):
    while "script>" in htmlAsString and "</script>" in htmlAsString:
        startOfScript = htmlAsString.find("script>")
        endOfScript = htmlAsString.find("</script>")
        htmlAsString = htmlAsString.replace(htmlAsString[startOfScript:endOfScript + 9], "")
    return htmlAsString


def removeHTMLTagsFromText(text):
    clean = re.compile("<.*?>")
    return re.sub(clean, "", text)
