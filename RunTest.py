import os
import json

TEST_PASSED = 0
testPass = True

with open('testList.json') as file:
    testList = json.load(file)

for test in testList:
    url = test['url']
    string = test['string']

    if " " in string:
        string = '"' + string + '"'

    os.system("echo " + str(test))
    testResultCode = os.system("pytest -v --url=" + str(url) + " --string=" + str(string) + " test.py")

    if testResultCode != TEST_PASSED:
        testPass = False

if testPass:
    os.system("echo ============= test passed =============")
    os.system("exit " + str(TEST_PASSED))
else:
    os.system("echo ============= test failed ============")
    raise Exception('Test Fail')
