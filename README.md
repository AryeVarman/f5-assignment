checking url resources for strings:

This project tests for a group of url and string pairs whether or not the string appears in the resource of the url,
for that we have the following files:

1. testList.json - a json file that contains all of the url, string pairs in a json format of {url, string}.

2. .gitlab-ci.yml - a YAML file that configures and runs the test - from setting the docker,
running the tests and cleaning up at the end.

3. RunTest.py - a python script that reads and parse the json file, activates the test for each {url, string} pair and
returns whether or not we passed the test (to pass the test all of the strings must be in the corresponding urls).

4. conftest.py - a configuration file for pytest - allows pytest to accept the url and string
arguments from the command line (in our case from RunTest.py).

5. test.py - gets url and string as arguments and uses pageChecker.py to assert if the string appears in the url resource.

6. pageChecker.py - does the logic behind the assertion of test.py - get the resource
strips it from "background noises" and checks if the string appears in it.

